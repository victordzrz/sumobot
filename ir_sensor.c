/*
 * ir_sensor.c
 *
 *  Created on: 03/03/2013
 *      Author: Víctor Díez
 *      		Beatriz Zurera
 */

#include"ir_sensor.h"
#include<avr/io.h>
#include<avr/delay.h>

void init_IR() {
	DDRB |= 1 << PB4 | 1 << PB7;
	//CTC, no prescaler -> 38.5kHz
	TCCR0A |= 1 << WGM01 | 1 << CS00;
	TCCR2A |= 1 << WGM21 | 1 << CS20;

	OCR0A = 103;
	OCR2A = 103;

}

//Returns true if it detects something
int left_input() {
	int in;
	PORTB|=1<<PB3;
	//turn on the IR LED
	TCCR0A |= 1 << COM0A0;
	_delay_us(500);
	//read the sensor output
	in = bit_is_clear(PINB,PB3);
	TCCR0A &= ~(1 << COM0A0);

	return in;
}

//Returns true if it detects something
int right_input() {
	int in;
	PORTB|=1<<PB2;
	//turn on the IR LED
	//TCCR2A |= 1 << COM2A0; USING JUST 1 LED IN PB4 / GREEN LINE
	TCCR0A |= 1 << COM0A0;
	_delay_us(500);
	//read the sensor output
	in = bit_is_clear(PINB,PB2);
	//TCCR2A &= ~(1 << COM2A0);
	TCCR0A &= ~(1 << COM0A0);

	return in;
}

