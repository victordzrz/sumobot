#include<avr/io.h>
#include"movement_control.h"

#define MAX 20480
#define TOP 19480
#define BOT 18480
#define MID 18980

void setup_movement() {
	DDRB |= 1 << PB5 | 1 << PB6;

	TCCR1A |= 1 << WGM11 | 1 << COM1A1 | 1 << COM1A0 | 1 << COM1B1 | 1 << COM1B0;
	TCCR1B |= 1 << WGM13 | 1 << WGM12 | 1 << CS11;

	ICR1 = MAX;
}

void move_bw() {
	OCR1A = TOP;
	OCR1B = BOT;
}

void move_fw() {
	OCR1A = BOT;
	OCR1B = TOP;
}

void stop() {
	OCR1A = MID;
	OCR1B = MID;

}

void rotate_right() {
	OCR1A = BOT;
	OCR1B = BOT;
}

void rotate_left() {
	OCR1A = TOP;
	OCR1B = TOP;
}

