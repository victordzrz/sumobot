/*
 * qti_sensor.c
 *
 *  Created on: 13/02/2013
 *      Author: Víctor Díez Rodríguez
 *      		Beatriz Zurera
 */
#include"qti_sensor.h"
#include<avr/io.h>
#include<util/delay.h>

int left_on_white() {
	DDRB |= 1 << PB0;
	PORTB |= 1 << PB0;
	_delay_ms(1);
	DDRB &= ~(1 << PB0);
	PORTB &= ~(1 << PB0);
	_delay_ms(1);
	if (bit_is_set(PINB,PB0)) {
		return 0;
	} else
		return 1;
}

int right_on_white() {
	DDRB |= 1 << PB1;
	PORTB |= 1 << PB1;
	_delay_ms(1);
	DDRB &= ~(1 << PB1);
	PORTB &= ~(1 << PB1);
	_delay_ms(1);
	if (bit_is_set(PINB,PB1)) {
		return 0;
	} else
		return 1;
}


