/*
 * ir_sensor.c
 *
 *  Created on: 03/03/2013
 *      Author: Víctor Díez
 *      		Beatriz Zurera
 */

#ifndef IR_SENSOR_H_
#define IR_SENSOR_H_

void init_IR();
int left_input();
int right_input();



#endif /* IR_SENSOR_H_ */
