/*
 * qti_sensor.h
 *
 *  Created on: 13/02/2013
 *      Author: Víctor Díez
 *      		Beatriz Zurera
 */

#ifndef QTI_SENSOR_H_
#define QTI_SENSOR_H_

int left_on_white();
int right_on_white();

#endif /* QTI_SENSOR_H_ */
