/*
 * movement_control.h
 *
 *  Created on: 7/02/2013
 *      Author: Víctor Díez
 *      		Beatriz Zurera
 */

#ifndef MOVEMENT_CONTROL_H_
#define MOVEMENT_CONTROL_H_

void setup_movement();
void move_fw();
void move_bw();
void rotate_left();
void rotate_right();
void stop();


#endif /* MOVEMENT_CONTROL_H_ */
