/*
 * main.c
 *
 *  Created on: 13/02/2013
 *      Author: Víctor Díez
 *      		Beatriz Zurera
 */

#include"movement_control.h"
#include"qti_sensor.h"
#include"ir_sensor.h"
#include<util/delay.h>

int main() {
	_delay_ms(5000);
	setup_movement();
	init_IR();
	while (1) {

		if (right_input() && left_input()) {
			move_bw();
			_delay_ms(100);
		} else if (/**right_on_white() ||**/left_input()) {
			rotate_right();
			_delay_ms(100);
		} else if (/**right_on_white() ||**/right_input()) {
			rotate_left();
			_delay_ms(100);
		} else {
			move_fw();
		}
	}
	while (1);
	return 0;
}

